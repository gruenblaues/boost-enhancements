#ifndef TRIAL_PROTOCOL_JSON_SERIALIZATION_IARCHIVE_HPP
#define TRIAL_PROTOCOL_JSON_SERIALIZATION_IARCHIVE_HPP

///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2015 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <list>
#include <boost/archive/detail/common_iarchive.hpp>
#include <boost/archive/detail/register_archive.hpp>
#include <trial/protocol/json/reader.hpp>

namespace trial
{
namespace protocol
{
namespace json
{

template <typename CharT>
class basic_iarchive
    : public boost::archive::detail::common_iarchive< basic_iarchive<CharT> >
{
    friend class boost::archive::load_access;

public:
    using value_type = CharT;

    basic_iarchive(const json::reader&);
    basic_iarchive(const json::reader::view_type&);
    template <typename Iterator>
    basic_iarchive(Iterator begin, Iterator end);


	// not possible, because not possible to search in unordered list to find a lement, while searching same list with different types.
	// it is necessary to have the template-function calls!
	//template< typename ... Types>
	//void insert(const Types& ...wrapArgs) {
	//	std::vector<any> wraps = { wrapArgs... };
	//	for (const auto &wrap : wraps)
	//		_wraps.push_back(reinterpret_cast<const boost::serialization::nvp<int>*>(&wrap));
	//}

	bool end() { 
		bool isFault = false;
		if (_isJsonCursorStopped)
		{
			std::stringstream ss;
			for (const auto &wrapEl : _wraps)
				ss << std::get<0>(wrapEl)->name() << ",";

			if (reader.category() != trial::protocol::json::token::category::data)
			{
				isFault = true;
				if (reader.category() == trial::protocol::json::token::category::status)
				{
					std::cerr << "JSON stream corrupt! It was only possible to read following variables: " << ss.str() << "!" << std::endl;
				}	// next object perhapse inside a array
			}
			else 
			if (_isRepeatNecessary)
			{
				if (_repeatCount > 0)
				{		// omit the step
					std::cerr << "Variable name '" << _currentVarName
						<< "', from JSON stream, was not found in serialization class with vars: " << ss.str() << "! Continue with missing value for this class." << std::endl;
					reader.next();
					_isJsonCursorStopped = false;
					_repeatCount = 0;
				}
				else {
					++_repeatCount;
				}
			}
		}
		
		bool isEnd = !_isRepeatNecessary || isFault; 
		_isRepeatNecessary = false; 
		if (isEnd) clear();
		return isEnd; 
	}

	void clear() {
		_wraps.clear();
		_isRepeatNecessary = true;
		_isJsonCursorStopped = false;
	}


	// the & operator 
	//original:
	//template<class T>
	//Archive & operator&(T & t) {
	//	return *(this->This()) >> t;
	//}
	template <typename T>
	basic_iarchive & operator+(boost::serialization::nvp<T> const& wrap) {
		if (reader.category() == trial::protocol::json::token::category::data)
		{
			// no var without a name
			if (!_isJsonCursorStopped) extractVarName();
			if (!_currentVarName.empty())
			{
				auto currName =  _currentVarName;		// var only necessary, because lambda construct can not reference *this in C++11
				// checking if var is already parsed, because it can be that the &-list is missing some variable or has one too-much.
				// Therefore, it is necessary to identify directly by name!
				if (std::find_if(_wraps.begin(), _wraps.end(),
					[&currName](const decltype(_wraps)::value_type &val)
				{
					return (std::get<0>(val)->name() == currName);
				}) == _wraps.end())
				{
					if (wrap.name() == _currentVarName)
					{
						*(this->This()) >> wrap.value();					// solve it in the moment of first time
						_wraps.push_back(std::make_tuple(reinterpret_cast<const boost::serialization::nvp<int>*>(&wrap), true));
						_isJsonCursorStopped = false;
						_repeatCount = 0;
					}
					else {
						_isJsonCursorStopped = true;
						_isRepeatNecessary = true;
					}
				}
			}
			else {
				std::cerr << "variable-name expected close to var '" << wrap.name() << "' in JSON stream!" << std::endl;
				_isRepeatNecessary = false;		// stop, because stream is corrupt!
			}
		}
		else {
			_isJsonCursorStopped = true;
			_isRepeatNecessary = false;			// stop, because parsing error occured
		}
		return *(this->This());
	}


    template<typename T>
    void load_override(T& data);

    template<typename T>
    void load_override(T& data, long);

    template <typename Tag>
    void load();

    template <typename T>
    void load(T&);

    template <typename Tag>
    bool at() const;

    token::code::value code() const;
    token::symbol::value symbol() const;
    token::category::value category() const;

#ifndef BOOST_DOXYGEN_INVOKED
    // Ignore these
    void load(boost::archive::version_type&) {}
    void load(boost::archive::object_id_type) {}
    void load(boost::archive::object_reference_type) {}
    void load(boost::archive::class_id_type) {}
    void load(boost::archive::class_id_optional_type) {}
    void load(boost::archive::class_id_reference_type) {}
    void load(boost::archive::tracking_type) {}
    void load(boost::archive::class_name_type&) {}
#endif

#ifndef BOOST_DOXYGEN_INVOKED
private:
    void next();
    void next(token::code::value);

	std::string & extractVarName()
	{
		if (reader.code() == trial::protocol::json::token::code::string)
		{
			_currentVarName = reader.value<std::string>();
			reader.next(token::code::string);
		}
		else {
			_currentVarName.clear();
		}
		return _currentVarName;
	}

private:
    json::basic_reader<value_type>	reader;
	std::string						_currentVarName;
	std::list<std::tuple<const boost::serialization::nvp<int> *, bool> >	_wraps;		//!<< contains handled variables
	bool							_isRepeatNecessary = true;
	int								_repeatCount = 0;
	bool							_isJsonCursorStopped = false;
#endif
};

using iarchive = basic_iarchive<char>;

} // namespace json
} // namespace protocol
} // namespace trial

#include <trial/protocol/json/serialization/detail/iarchive.ipp>
#include <boost/archive/detail/register_archive.hpp>

BOOST_SERIALIZATION_REGISTER_ARCHIVE(trial::protocol::json::iarchive)

#endif // TRIAL_PROTOCOL_JSON_SERIALIZATION_IARCHIVE_HPP
